﻿Shader "Stephen/Grabpass/Distort"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_TintColor ("Main Color", color) = (0.5,0.5,0.5,0.5)
		_NormalTex ("Normal Texture", 2D) = "bump" {}
		_NormalAmount("Distortion", range(-128,128)) = 16
	}
    SubShader
    {
        // Draw ourselves after all opaque geometry
        Tags { "RenderType"="Transparent" "Queue"="Transparent-1" }
		ZWrite Off
		ZTest Always
		//Offset 0, -10000000
		Blend SrcAlpha OneMinusSrcAlpha

        // Grab the screen behind the object into _BackgroundTexture
        GrabPass
        {
            "_DistortTexture"
        }

        // Render the object with the texture generated above, and invert the colors
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float4 color : COLOR;
			};

            struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float3 worldPos : TEXCOORD1;
				float4 grabPos : TEXCOORD2;
 				float4 color : COLOR;
			};

			
            sampler2D _DistortTexture;
			float4 _DistortTexture_TexelSize;

			sampler2D _MainTex;
			sampler2D _NormalTex;
			float4 _MainTex_ST;
			float4 _TintColor;

			float _NormalAmount;

            v2f vert(appdata v) 
			{
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos.xyz = mul(unity_ObjectToWorld, v.vertex);
				o.normal = v.normal;	
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
                o.grabPos = ComputeGrabScreenPos(o.pos);
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
				half4 col = tex2D(_MainTex, i.uv);
				half2 norm = UnpackNormal(tex2D(_NormalTex, i.uv)).rg;

				float2 offset = saturate(col.a) * norm * _NormalAmount * _DistortTexture_TexelSize.xy * 100.0f;
				i.grabPos.xy += offset;
                //half4 bgcolor = tex2Dproj(_DistortTexture, i.grabPos);

				
				half4 grabCol = tex2Dproj(_DistortTexture, UNITY_PROJ_COORD(i.grabPos));

				col.rgb *= grabCol.rgb;
				col *= _TintColor * unity_ColorSpaceDouble;
				col *= i.color;

				return col;
				
                //return 1 - bgcolor;
            }
            ENDCG
        }

    }
}