﻿Shader "Custom/CloudSurface" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_CloudTex("Cloud", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_ScrollSpeed ("Scroll Speed", Range(-24,24)) = 0.0
		_Cutoff ("Alpha Cutoff", Range(0,1)) = 0.5
		_BurnTex ("Burn (RGB)", 2D) = "white" {}
		[HDR]_BurnCol("Burn Color", Color) = (1,1,1)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		//Cull Off

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _CloudTex;
		sampler2D _BurnTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv_CloudTex;
		};

		half _Glossiness;
		half _Metallic;
		float _ScrollSpeed;
		float _Cutoff;
		fixed4 _Color;
		float3 _BurnCol;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			clip(c.a - 0.5);
			o.Albedo = c.rgb;
			float transp = tex2D (_CloudTex, IN.uv_CloudTex + _Time.r * _ScrollSpeed)
			+ tex2D (_CloudTex, IN.uv_CloudTex - _Time.r * _ScrollSpeed);
			transp *= 0.5;

			transp = transp - (1.01 - _Cutoff * 1);
			o.Emission = _BurnCol * tex2D (_BurnTex, 1-transp.xx).rgb;
			clip(transp);
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
