﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/RainShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _RainTex;
			float _Timer;
			float _Amount;
			float _Halfway;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 aspectRatio = float2(_ScreenParams.x / _ScreenParams.y, 1.0f);
				//float
				float2 uv = i.uv;
				uv.y += _Time.r * 4.0f;
				float3 rainUV = tex2D(_RainTex, (uv) * aspectRatio + 0.5f).xyz;
				rainUV.xy = rainUV.xy + rainUV.xy - 1.0f;
				float timer;
				if (_Timer < _Halfway)
				{
					timer = saturate(lerp(0.0, 1.0, _Timer / _Halfway - rainUV.z));
				}
				else
				{
					timer = saturate(lerp(1.0, 0.0, (_Timer - (_Halfway)) * (1.0f + _Halfway) + rainUV.z));
				}

				//fixed4 col = tex2D(_MainTex, i.uv + rainUV.xy * _Amount);
				fixed4 col = tex2D(_MainTex, i.uv - rainUV.xy * timer * _Amount);
				
				return col;
			}
			ENDCG
		}
	}
}
