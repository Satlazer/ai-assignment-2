﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RainScript : MonoBehaviour
{
    public Shader shader;
    public Texture rainTexture;

    Material effectMaterial;

    float time = 0.0f;
    float speed = 0.5f;
    bool active = false;
    public float length = 2.25f;

    float amount = 0.0f;
    float maxAmount = 0.125f;

	// Use this for initialization
	void Start ()
    {
		effectMaterial = new Material(shader);
	}
	
    public void ActivateRain()
    {
        time = 0.0f;
        active = true;
    }

	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            ActivateRain();
        }

		if (active)
        {
            time += Time.deltaTime * speed;
            amount = Mathf.Clamp(1.0f - time / length, 0.0f, 1.0f);

            if(time > length)
            {
                active = false;
                amount = 0.0f;
            }
        }
	}

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        
        effectMaterial.SetFloat("_Timer", time / length);
        effectMaterial.SetFloat("_Halfway", 0.1f);
        effectMaterial.SetTexture("_RainTex", rainTexture);
        effectMaterial.SetFloat("_Amount", maxAmount * amount); //  * Mathf.Sin(Time.time * 4.0f)

        Graphics.Blit(source, destination, effectMaterial);
    }
}
