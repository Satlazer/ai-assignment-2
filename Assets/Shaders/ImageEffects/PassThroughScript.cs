﻿using UnityEngine;
using System.Collections;

[ImageEffectAllowedInSceneView]
[ExecuteInEditMode]
public class PassThroughScript : MonoBehaviour
{
    public Shader shader;
    public Texture texture;
    Material material;

    // Use this for initialization
    void Start()
    {
        material = new Material(shader);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetTexture("_Tex" , texture);
        Graphics.Blit(source, destination, material);
    }
}
