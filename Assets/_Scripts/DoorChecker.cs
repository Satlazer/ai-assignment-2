﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class DoorChecker : MonoBehaviour 
{

	
    QueryTriggerInteraction hitTriggers;
    public LayerMask whatToCollideWith;
    RaycastHit hit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("Menu", LoadSceneMode.Single);
		}

		if(Input.GetMouseButtonDown(0))
		{
			Ray ray = new Ray(transform.position, transform.forward);

        	float currDistanceMax = 5.0f;

        	//line.SetPosition(0, ray.origin);
        	if (Physics.Raycast(ray, out hit, currDistanceMax, whatToCollideWith, hitTriggers))
        	{            
				Debug.Log("Door HIT!");
				Door door = hit.collider.gameObject.GetComponentInParent<Door>();
				if(door != null)
				{
					door.OpenDoor();
					Debug.Log("AHAHAHA");
				}

        	}
		}
	}
}
