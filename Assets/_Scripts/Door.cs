﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Door : MonoBehaviour
{
	// 4 2 1 - Base 2 Int
	// 1 1 1 - 0 0 0
	// Y Y Y - N N N
	static int doorsOpened = 0;
	public int doorType = 0;
	public bool hot = false;
	public bool noisy = false;
	public bool safe = false;
	public AudioClip openMessage;
	public AudioSource audioSource;
	AudioChoices audioChoices;
	GameObject hotReference;
	GameObject noisyReference;
	GameObject safeReference;
	

	public GameObject doorHingeReference;
	public GameObject doorInsideReference;
	public GameObject doorInsideBackReference;

	
	public GameObject hotPrefab; 
	public GameObject noisyPrefab; 
	public GameObject safePrefab; 
	public GameObject hotNotPrefab; 
	public GameObject noisyNotPrefab; 
	public GameObject safeNotPrefab; 

	float timeOpen = 0.0f;
	float cutoff = 1.0f;
	bool open = false;

	int disappearOption;

	public void GeneratePrefabs()
	{
		audioChoices = GameObject.Find("AudioChoices").GetComponent<AudioChoices>();
		if(hot)
			hotReference = Instantiate(hotPrefab, transform.position, transform.rotation);			
		else
			hotReference = Instantiate(hotNotPrefab, transform.position, transform.rotation);			
		if(noisy)
			noisyReference = Instantiate(noisyPrefab, transform.position, transform.rotation);			
		else
			noisyReference = Instantiate(noisyNotPrefab, transform.position, transform.rotation);
		if(safe)
			safeReference = Instantiate(safePrefab, transform.position, transform.rotation);			
		else
			safeReference = Instantiate(safeNotPrefab, transform.position, transform.rotation);	

		if(safe)
			openMessage = audioChoices.SafeDoors[Random.Range(0, audioChoices.SafeDoors.Length)];
		else
			openMessage = audioChoices.UnsafeDoors[Random.Range(0, audioChoices.UnsafeDoors.Length)];

			
		if(safe)
		{		
			Vector4 SafeColor = new Vector4(0.0f, 6.0f, 0.0f, 0.0f);
			doorInsideReference.GetComponent<MeshRenderer>().material.SetColor("_BurnCol", SafeColor);
			doorInsideBackReference.GetComponent<MeshRenderer>().material.SetColor("_BurnCol", SafeColor);
		}

	}	

	public void OpenDoor()
	{
		if(!open)
		{
			open = true;

			audioSource.PlayOneShot(openMessage);
			//if(noisy)
			hotReference.GetComponentInChildren<AudioSource>().Stop();
			noisyReference.GetComponentInChildren<AudioSource>().Stop();

			disappearOption = Random.Range(1,7);
			if(doorsOpened < 3)
			{
				disappearOption = -1;
			}

			if(safe)
				safeReference.GetComponentInChildren<ParticleSystem>().Emit(200);
				
			++doorsOpened;
		}
	}

	void FixedUpdate()
	{
		if(open)
			timeOpen += Time.fixedDeltaTime;

		if(cutoff <= 0.0f)
		{
			Destroy(doorInsideReference);
		}
		else
		{
			// DisappearOption determines the hardcoded animation that the door will take, picked randomly when opened 
			if(disappearOption == -1)
			{				
				cutoff = 1.25f-timeOpen * 0.25f;				
				doorHingeReference.transform.rotation = Quaternion.Slerp(doorInsideReference.transform.rotation, Quaternion.identity * Quaternion.AngleAxis(150.0f, Vector3.up), 0.015f);
			}
			else if(disappearOption == 1)
			{
				cutoff = 1.0f-timeOpen;
			}
			else if(disappearOption == 2)
			{
				cutoff = 1.0f-timeOpen * 0.25f;
				doorHingeReference.transform.Rotate(0.0f, -Time.fixedDeltaTime * 100.0f * Mathf.Pow(timeOpen, 4.0f), 0.0f);
				doorHingeReference.transform.Translate(0.0f, -Time.fixedDeltaTime * Mathf.Pow(timeOpen, 2.0f) * 0.25f, 0.0f);
			}
			else if(disappearOption == 3)
			{
				cutoff = 1.0f-timeOpen * 0.35f;
				doorHingeReference.transform.localScale = new Vector3(1.0f, 1.0f / (5.0f * timeOpen+1.0f), 1.0f);
			}
			else if(disappearOption == 4)
			{		
				cutoff = 1.0f-timeOpen * 0.5f;	
				doorInsideReference.transform.Rotate(0.0f, 0.0f, -Time.fixedDeltaTime * 250.0f);
				doorInsideReference.transform.Translate(0.0f, 0.0f, Time.fixedDeltaTime);
			}
			else if(disappearOption == 5)
			{		
				cutoff = 1.0f-timeOpen * 0.5f;	
				doorInsideReference.transform.Translate(0.0f, Time.fixedDeltaTime, Time.fixedDeltaTime * 0.05f) ;
			}
			else if(disappearOption == 6)
			{		
				cutoff = 1.0f-timeOpen * 0.5f;				
				doorHingeReference.transform.rotation = Quaternion.Slerp(doorInsideReference.transform.rotation, Quaternion.identity * Quaternion.AngleAxis(90.0f, Vector3.right), 0.05f);
			}

			if(disappearOption!=0)
			{
				cutoff = Mathf.Clamp01(cutoff);
				doorInsideReference.GetComponent<MeshRenderer>().material.SetFloat("_Cutoff", cutoff);
				doorInsideBackReference.GetComponent<MeshRenderer>().material.SetFloat("_Cutoff", cutoff);
			}
		}

		
	}

	void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}
}

public class DoorType
{
	int doorType = 0;
	public bool hot = false;
	public bool noisy = false;
	public bool safe = false;
	public float probability = 0.0f;
	public float probabilitySum = 0.0f;
}