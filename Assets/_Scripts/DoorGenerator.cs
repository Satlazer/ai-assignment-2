﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DoorGenerator : MonoBehaviour 
{
	public GameObject doorPrefab; 
	public GameObject hotPrefab; 
	public GameObject noisyPrefab; 
	public GameObject safePrefab; 
	public GameObject hotNotPrefab; 
	public GameObject noisyNotPrefab; 
	public GameObject safeNotPrefab; 

	public List<Door> Doors;
	public int numberOfDoors = 500;
	public List<DoorType> _DoorTypes;
	
	public float[] probability = new float[8];
	// probabiltySum is the threshold of the probility of this door type plus all previous doors 
	// Ex. if door1 is 0.5 and door2 is 1.0, the probabilitySum for door2 is 1.5 
	// So each probabilitySum is the probability of this door plus the previous probabilitySum
	public float[] probabilitySum = new float[8];

	private float[] percentageOfActual = new float[8];

	//		Assets/Data/data.txt
	public string path = "data.txt";

	// Use this for initialization
	

	// Get the number from the end of the string using a stack
	// Keep going back through the string until a non number value is found
	// Convert the stack to a string, and then into a number
	float GetNumberAtEnd(string input)
	{
		Stack<char> stack = new Stack<char>();

		for (var i = input.Length - 1; i >= 0; i--)
		{
		    if (!char.IsNumber(input[i]) && input[i] != '.')
		        break;

		    stack.Push(input[i]);
		}

		return float.Parse(new string(stack.ToArray()));
	}

	void Start () 
	{
		_DoorTypes = new List<DoorType>();
		Doors = new List<Door>();
		
        StreamReader reader;
		if (FilePicker.filePath == null || !System.IO.File.Exists(FilePicker.filePath))
		{
			Debug.Log("File Does Not Exist!");
        	reader = new StreamReader(path); 
		}
		else
			reader = new StreamReader(FilePicker.filePath); 
		// Ignore that first line with all the gibberish
		reader.ReadLine();
		
		float sum = 0.0f;
		for(int i = 7; i >= 0; --i)
		{
			// Okay, so we get that line and make it a float for each type
			float number = GetNumberAtEnd(reader.ReadLine());
			probability[i] = number;
			sum += number;
		}
		
        reader.Close();

		// If the sum of the probabilities does not equal 1, something is wrong, 
		// So we gotta take all the numbers and divide them by the sum to normalize them
		if(sum != 1.0f)
		{
			print("Sum of probabilities does not equal 1! \nNormalizing!");
			for(int i = 0; i < 8; ++i)
			{
				probability[i] /= sum;
			}
		}

		// probabiltySum is the threshold of the probility of this door type plus all previous doors 
		// Ex. if door1 is 0.5 and door2 is 1.0, the probabilitySum for door2 is 1.5 
		// So each probabilitySum is the probability of this door plus the previous probabilitySum
		probabilitySum[0] = probability[0];
		for(int i = 1; i < 8; ++i)
		{
			probabilitySum[i] = probabilitySum[i-1] + probability[i];
		}

		for(int i = 0; i < 8; ++i)
		{
			// Convert the line number to a door type
			// Use base 2 AND bitwise operators to check each type
			// Ex. 5 is 0b101, so it is hot and safe
			DoorType type = new DoorType();
			type.hot   = (i & 0x4) > 0;
			type.noisy = (i & 0x2) > 0;
			type.safe  = (i & 0x1) > 0;
			type.probability = probability[i];
			type.probabilitySum = probabilitySum[i];
			

			Debug.Log(probabilitySum[i] + "\t" + type.hot + " " + type.noisy + " " + type.safe);
			_DoorTypes.Add(type);
		}


		GenerateDoors();
	}


	void GenerateDoors()
	{
		for(int i = 0; i < numberOfDoors; ++i)
		{
			// Generate a number from 0-1
			// Compare this number to the sum of all previous probabilities for each door type
			// The first door type starts at 0
			// It will advance through the for loop until it finds a probability that is less than it
			float randomDoorType = Random.Range(0.0f, 1.0f);
			int doorType = 0;
			for(int j = 1; j < 8; ++j)
			{
				if(randomDoorType > probabilitySum[j-1])
					doorType = j;
				else
					break;
			}

			// Position the doors so that they create an alley
			// Rotate every odd one
			Vector3 doorPosition = new Vector3(i * 2.0f, 0.0f, 5.0f);
			Quaternion doorRotation = Quaternion.identity;
			if(i % 2 == 1)
			{
				doorPosition = new Vector3(i * 2.0f - 2.0f, 0.0f, -5.0f);
				doorRotation = doorRotation * Quaternion.AngleAxis(180.0f, Vector3.up);
			}

			// Create the door and initialize them
			GameObject doorGameObject = Instantiate(doorPrefab, doorPosition, doorRotation);
			Door door = doorGameObject.GetComponent<Door>();
			door.doorType = doorType;
			door.hot = _DoorTypes[doorType].hot;
			door.noisy = _DoorTypes[doorType].noisy;
			door.safe = _DoorTypes[doorType].safe;
			door.GeneratePrefabs();
			Doors.Add(door);
		}
		calculatePercentage();
	}

	void calculatePercentage()
	{
		// Get the sum of a type of door divided by the sum of doors
		for(int i = 0; i < 8; ++i)
		{
			percentageOfActual[i] = 0.0f;
			for(int j = 0; j < numberOfDoors; ++j)
			{
				if(Doors[j].doorType == i)
				{
					percentageOfActual[i] += 1.0f;
				}
			}

			percentageOfActual[i] /= numberOfDoors;
		}
	}

	void OnGUI()
	{
		for(int i = 0; i < 8; ++i)
		{
			// Convert the integer into the three booleans
			string label =  
			((i & 0x4) > 0) + "\t" +
 			((i & 0x2) > 0) + "\t" +
 			((i & 0x1) > 0) + "\t" +
			 probability[i].ToString("F3") + "\t" +
			 percentageOfActual[i].ToString("F3");
			GUI.Label(new Rect(10, 20 + (7-i) * 15, 300, 20), label);
		}
		GUI.Label(new Rect(10, 5 , 300, 20), "Hot\tNoisy\tSafe\tPercent\tDistr.");


		string tutorial = "";
		tutorial += "Left Click to open doors\n";
		tutorial += "Right Click to ?????\n";
		tutorial += "Esc to Quit\n";
		GUI.Label(new Rect(Screen.width - 200 - 10, 5 , 200, 40), tutorial);
	}
}


