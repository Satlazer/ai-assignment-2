using System.Collections;
using UnityEngine;
using SFB;

using UnityEngine.SceneManagement;

public class FilePicker : MonoBehaviour 
{
    public static string filePath;
    
    private string _path;
    public Texture buttonIcon;

    void Update()
    {        
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
    }

    void OnGUI() {
        var guiScale = new Vector3(Screen.width / 800.0f, Screen.height / 600.0f, 1.0f);
        GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, guiScale);

        GUILayout.Space(20);
        GUILayout.BeginHorizontal();
        GUILayout.Space(20);
        GUILayout.BeginVertical();

        // Open File Samples

        if (GUILayout.Button(buttonIcon, "Open File")) 
        {
            WriteResult(StandaloneFileBrowser.OpenFilePanel("Open File", "", "", false));
            
            if (true)
            {
                //Debug.Log("File Exists!");
                UnityEngine.SceneManagement.SceneManager.LoadScene("Test", LoadSceneMode.Single);
		    }

		    //Debug.Log("File Does Not Exist!");
        }
       

        GUILayout.Space(15);


        GUILayout.EndVertical();
        GUILayout.Space(20);
        //GUILayout.Label(_path);
        GUILayout.EndHorizontal();
    }

    public void WriteResult(string[] paths) 
    {
        if (paths.Length == 0) {
            return;
        }

        _path = "";
        foreach (var p in paths) {
            _path += p + "\n";
        }

        filePath = _path;
        filePath = filePath.Replace("\n", "");
        filePath = filePath.Replace("\r", "");
        Debug.Log(filePath);
    }

    public void WriteResult(string path)
     {
        _path = path;
        filePath = _path;
    }
}
